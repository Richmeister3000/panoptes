package at.ac.tuwien.infosys.internal;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.api.ManagedElement;
import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.Execute;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class ExecuteService implements Execute, Subscriber<PlainEvent> {

	private KnowledgeBase knowledgeBaseService;
	private BundleContext context;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteService.class);

	private boolean reset;
	
	public ExecuteService() {
		reset = true;
	}
	
	public void init() {
		knowledgeBaseService.getEventBus().subscribe(PlainEvent.class, this);
	}
	
	public void destroy() {
		knowledgeBaseService.getEventBus().unsubscribe(PlainEvent.class, this);
	}
	
	public void act() {
		SensorConfig config = this.knowledgeBaseService.loadSensorConfig();
		if (reset) {
			config.setReset(true);
			reset = false;
		}
		if (config != null) {
			try {
				ServiceReference[] ref = context.getServiceReferences(ManagedElement.class.getName(), null);
				if (ref != null && ref.length == 1) {
					ManagedElement managedElement = (ManagedElement) context.getService(ref[0]);
					managedElement.effect(config);
				} else {
					LOGGER.warn("Did not find proper service reference!");
				}
			} catch (InvalidSyntaxException e) {
				LOGGER.error(e.getMessage());
				e.printStackTrace();
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBase) {
		this.knowledgeBaseService = knowledgeBase;
	}

	public BundleContext getContext() {
		return context;
	}

	public void setContext(BundleContext context) {
		this.context = context;
	}

	@Override
	public void notify(PlainEvent event) {
		if("ResetEvent".equals(event.getEvent())) {
			reset = true;
		}
	}
}
