package at.ac.tuwien.infosys.api.domain;

import java.io.Serializable;

public class PanoptesConfiguration implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int sampleSize;
	private double maxError;
	private double deviation;
	private long frequency;
	private long measurements;
	
	private boolean accurateDeviation;

	private int max;
	private int current;

	public PanoptesConfiguration() {
		this.reset();
	}
	
	public void reset() {
		this.sampleSize = 2;
		this.frequency = 1;
		this.measurements = 0;
		this.deviation = 0;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void connected() {
		current += 1;
	}
	
	public void setCurrent(int current) {
		this.current = current;
	}

	public int getSampleSize() {
		return sampleSize;
	}

	public void setSampleSize(int sampleSize) {
		this.sampleSize = sampleSize;
	}

	public double getMaxError() {
		return maxError;
	}

	public void setMaxError(double maxError) {
		this.maxError = maxError;
	}

	public double getDeviation() {
		return deviation;
	}

	public void setDeviation(double deviation) {
		this.deviation = deviation;
	}

	public long getFrequency() {
		return frequency;
	}

	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}

	public long getMeasurements() {
		return measurements;
	}

	public void addMeasurements(long measurements) {
		this.measurements += measurements;
	}

	public int getCurrent() {
		return current;
	}

	public boolean isAccurateDeviation() {
		return accurateDeviation;
	}

	public void setAccurateDeviation(boolean accurateDeviation) {
		this.accurateDeviation = accurateDeviation;
	}
}
