package at.ac.tuwien.infosys.api.events;

public class PlainEvent implements Event {

	private String event;
	
	public PlainEvent(String event) {
		this.event = event;
	}
	
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
}
