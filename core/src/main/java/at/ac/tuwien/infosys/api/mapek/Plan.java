package at.ac.tuwien.infosys.api.mapek;


/**
 * @author Richard Holzeis
 * Represents the P of the MAPE-K loop.
 */
public interface Plan {

	/**
	 * This method calculates an appropriate frequency for each sensor 
	 * according to the current state stored in the knowledge base.
	 * Therefore the distance and shannon's theorem will be taken in 
	 * concern.
	 */
	public void decide();
}
