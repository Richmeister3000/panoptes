package at.ac.tuwien.infosys.api.domain;

import weka.classifiers.Classifier;

public class Neighbour {

	private Classifier classifier;
	private int classIdx;
	private SensorState state;
	
	public Classifier getClassifier() {
		return classifier;
	}
	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}
	public int getClassIdx() {
		return classIdx;
	}
	public void setClassIdx(int classIdx) {
		this.classIdx = classIdx;
	}
	public SensorState getState() {
		return state;
	}
	public void setState(SensorState state) {
		this.state = state;
	}
}
