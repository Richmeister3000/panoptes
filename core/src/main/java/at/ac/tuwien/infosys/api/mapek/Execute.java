package at.ac.tuwien.infosys.api.mapek;

/**
 * @author Richard Holzeis
 * Represents the E of the MAPE-K loop.
 */
public interface Execute {

	/**
	 * This method acts according to the current state of the sensors and
	 * creates the adaptation for reconfigured sensors. These configurations
	 * will be save to the knowledge base.
	 */
	public void act();
}
