package at.ac.tuwien.infosys.api;

import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorData;

public interface AutonomicManager {

	/**
	 * This method triggers the feedback loop MAPE-K introduced by the IBM.
	 * @param sensorData represents the measurements of sensors.
	 * @return a configuration for each sensor.
	 */
	public void manage(List<SensorData> sensorData);
}
