package at.ac.tuwien.infosys.api.mapek;

import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorData;

/**
 * @author Richard Holzeis
 * Represents the M of the MAPE-K loop.
 */
public interface Monitor {

	/**
	 * This method collects monitored sensor values and updates the current state
	 * in the knowledge base. Afterwards the measured data will be chained to the
	 * analyze phase.
	 * @param sensorData represents the measured values.
	 */
	public void collect(List<SensorData> sensorData);
}
