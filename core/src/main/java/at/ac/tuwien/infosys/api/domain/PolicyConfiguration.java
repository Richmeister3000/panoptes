package at.ac.tuwien.infosys.api.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PolicyConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private String selectedPolicy;

	private List<Class<?>> policies;

	public String getSelectedPolicy() {
		if (selectedPolicy == null) {
			if (policies != null && policies.size() > 0) {
				return policies.get(0).getSimpleName();
			}
		}
		return selectedPolicy;
	}

	public void setSelectedPolicy(String selectedPolicy) {
		this.selectedPolicy = selectedPolicy;
	}

	public List<Class<?>> getPolicies() {
		return policies;
	}
	
	public List<String> getPoliciesAsString() {
		List<String> list = new ArrayList<String>();
		for (Class<?> clz : policies) {
			list.add(clz.getSimpleName());
		}
		return list;
	}

	public void setPolicies(List<Class<?>> policies) {
		this.policies = policies;
	}
}
