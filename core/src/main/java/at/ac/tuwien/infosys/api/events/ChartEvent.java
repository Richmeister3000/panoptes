package at.ac.tuwien.infosys.api.events;

public class ChartEvent implements Event {

	private Object chart;

	public Object getChart() {
		return chart;
	}

	public void setChart(Object chart) {
		this.chart = chart;
	}
}
