package at.ac.tuwien.infosys.api;

import at.ac.tuwien.infosys.api.domain.SensorConfig;

public interface ManagedElement {

	/**
	 * This method is responsible to reconfigure its behavior of sending sensor measurements
	 * according to the passed configuration. 
	 * @param configuration represent the autonomic decision done by the MAPE-K loop and
	 * contains reconfiguration information for declared sensors.
	 */
	public void effect(SensorConfig config);
}
