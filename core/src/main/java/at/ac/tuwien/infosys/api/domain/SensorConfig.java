package at.ac.tuwien.infosys.api.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.infosys.api.util.HashCodeUtil;

public class SensorConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> sensors;
	private boolean reset;
	
	private int fHashCode;
	
	public SensorConfig() {
		this.sensors = new ArrayList<String>();
	}
	
	public List<String> getSensors() {
		return sensors;
	}

	public void addSensor(String sensor) {
		this.sensors.add(sensor);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SensorData))
			return false;
		return ((SensorData) o).hashCode() == this.hashCode();
	}
	
	@Override
	public int hashCode() {
		if (fHashCode == 0) {
			int result = HashCodeUtil.SEED;
			result = HashCodeUtil.hash(result, sensors.hashCode());
			fHashCode = result;
		}
		return fHashCode;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}
}
