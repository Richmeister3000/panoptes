package at.ac.tuwien.infosys.api.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.ac.tuwien.infosys.api.util.HashCodeUtil;

public class SensorState implements Serializable {

	private static final long serialVersionUID = 1L;

	private String sensor;

	private long lastMeasured;
	private double lastMeasurement;

	private int measured;

	private List<Double> calculation;
	private List<Double> error;
	
	private double lastError;
	
	private int index;
	private static final int WINDOW_MAX = 25;

	private long lastCalculated;

	private int fHashCode;

	public SensorState() {
		calculation = new ArrayList<Double>();
		error = new ArrayList<Double>();
		measured = 0;
		lastMeasurement = -1;
		lastError = 0;
	}

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	public double getLastMeasurement() {
		return lastMeasurement;
	}

	public void setLastMeasurement(double newMeasurement) {
		measured++;
		lastMeasurement = newMeasurement;
	}

	public List<Double> getCalculation() {
		return calculation;
	}

	public void addCalcMeasurment(Double calc) {
		this.calculation.add(calc);
	}

	public void resetCalculation() {
		this.calculation.clear();
	}

	public double calculationMean() {
		if (this.getCalculation().size() == 0)
			return 0;
		double calc = 0;
		for (Double calculation : this.getCalculation()) {
			calc += calculation;
		}

		return calc / this.getCalculation().size();
	}

	public Double calculationMost() {
		if (this.getCalculation().size() == 0)
			return null;
		double calc = 0;
		int frequency = 0;
		for (Double val : this.getCalculation()) {
			int freq = Collections.frequency(this.getCalculation(), val);
			if (frequency < freq) {
				calc = val;
				frequency = freq;
			}
		}
		return calc;
	}

	public long getLastMeasured() {
		return lastMeasured;
	}

	public void setLastMeasured(long lastMeasured) {
		this.lastMeasured = lastMeasured;
	}

	public long getLastCalculated() {
		return lastCalculated;
	}

	public void setLastCalculated(long lastCalculated) {
		this.lastCalculated = lastCalculated;
	}

	public double getError() {
		if (error.size() == 0)
			return 0;
		double failure = 0;
		for (int i = 0; i < error.size(); i++) {
			failure += error.get(i);
		}
		return failure / error.size();
	}

	public void addFailure(double failure) {
		if (this.error.size() <= index) {
			this.error.add(failure);
		} else {
			this.error.set(index, failure);
		}
		lastError = failure;
		index = (index == WINDOW_MAX) ? 0 : index + 1;
	}

	@Override
	public int hashCode() {
		if (fHashCode == 0) {
			int result = HashCodeUtil.SEED;
			result = HashCodeUtil.hash(result, sensor.hashCode());
			fHashCode = result;
		}
		return fHashCode;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SensorState))
			return false;
		return ((SensorState) o).hashCode() == this.hashCode();
	}

	public int getMeasured() {
		return measured;
	}

	public void reset(boolean reset) {
		error.clear();
		error.add(lastError);
		this.measured = 0;
		
		if (reset) {
			this.lastMeasurement = -1;
			this.index = 0;
			this.lastCalculated = 0;
			this.lastMeasured = 0;
			this.calculation.clear();
			this.lastError = 0;
		}
	}
}