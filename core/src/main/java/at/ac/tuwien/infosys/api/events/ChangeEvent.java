package at.ac.tuwien.infosys.api.events;

import at.ac.tuwien.infosys.api.domain.SensorState;

public class ChangeEvent implements Event {

	private SensorState source;
	
	private double oldMeasurement;
	private double newMeasurement;
	
	private double error;

	public double getOldMeasurement() {
		return oldMeasurement;
	}

	public void setOldMeasurement(double oldMeasurement) {
		this.oldMeasurement = oldMeasurement;
	}

	public double getNewMeasurement() {
		return newMeasurement;
	}

	public void setNewMeasurement(double newMeasurement) {
		this.newMeasurement = newMeasurement;
	}

	public double getError() {
		return error;
	}

	public void setError(double error) {
		this.error = error;
	}

	public SensorState getSource() {
		return source;
	}

	public void setSource(SensorState source) {
		this.source = source;
	}
}