package at.ac.tuwien.infosys.api;

import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.Subscriber;

public interface EventBus {
	
	public void subscribe(Class<? extends Event> clazz, Subscriber<?> subscriber);
	
	public void unsubscribe(Class<? extends Event> clazz, Subscriber<?> subscriber);

	public void publish(Event event);
	
}