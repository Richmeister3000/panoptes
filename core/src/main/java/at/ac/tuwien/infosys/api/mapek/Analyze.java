package at.ac.tuwien.infosys.api.mapek;

import at.ac.tuwien.infosys.api.SupervisedLearning;

/**
 * @author Richard Holzeis
 * Represents the A of the MAPE-K loop.
 */
public interface Analyze extends SupervisedLearning {

	/**
	 * This method analyzes the measured data and detects the distance
	 * to the learned cluster. This information will stored into the
	 * current state represented in the knowledge base.
	 * @param sensorData represents the measured values.
	 */
	public void analyze();
}
