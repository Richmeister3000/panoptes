package at.ac.tuwien.infosys.api.policy;

public abstract class FrequencyPolicy extends Policy {

	public abstract void frequency();
}
