package at.ac.tuwien.infosys.api;

import java.io.InputStream;

public interface SupervisedLearning {

	/**
	 * @param arffStream represents the training data according to the ARFF standard.
	 */
	public void train(InputStream arffStream);
}
