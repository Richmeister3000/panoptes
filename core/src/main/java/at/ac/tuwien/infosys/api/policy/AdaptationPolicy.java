package at.ac.tuwien.infosys.api.policy;

public abstract class AdaptationPolicy extends Policy {

	public abstract void adapt();
}
