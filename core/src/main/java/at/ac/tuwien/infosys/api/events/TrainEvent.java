package at.ac.tuwien.infosys.api.events;

import java.io.InputStream;

public class TrainEvent implements Event {

	private InputStream arffStream;

	public InputStream getArffStream() {
		return arffStream;
	}

	public void setArffStream(InputStream arffStream) {
		this.arffStream = arffStream;
	}
}
