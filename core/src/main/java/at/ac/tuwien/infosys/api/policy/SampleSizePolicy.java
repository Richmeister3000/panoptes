package at.ac.tuwien.infosys.api.policy;

public abstract class SampleSizePolicy extends Policy {

	public abstract void sample();
}
