package at.ac.tuwien.infosys.api.events;

public interface Subscriber<E> {

	public void notify(E event);
}
