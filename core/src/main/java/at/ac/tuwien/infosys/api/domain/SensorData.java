package at.ac.tuwien.infosys.api.domain;

import java.io.Serializable;

import at.ac.tuwien.infosys.api.util.HashCodeUtil;

public class SensorData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private double measurement;
	
	private int fHashCode;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getMeasurement() {
		return measurement;
	}
	public void setMeasurement(double measurement) {
		this.measurement = measurement;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SensorData))
			return false;
		return ((SensorData) o).hashCode() == this.hashCode();
	}
	
	@Override
	public int hashCode() {
		if (fHashCode == 0) {
			int result = HashCodeUtil.SEED;
			result = HashCodeUtil.hash(result, name.hashCode());
			fHashCode = result;
		}
		return fHashCode;
	}
}
