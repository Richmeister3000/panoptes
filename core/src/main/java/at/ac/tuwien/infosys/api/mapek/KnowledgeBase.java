package at.ac.tuwien.infosys.api.mapek;

import java.util.List;

import weka.classifiers.Classifier;
import at.ac.tuwien.infosys.api.EventBus;
import at.ac.tuwien.infosys.api.domain.Neighbour;
import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.PolicyConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.domain.SensorData;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.policy.Policy;

public interface KnowledgeBase {
	
	public void persistSnapshot(List<SensorData> sensorData);
	
	public List<SensorData> loadSnapshot();
	
	public void persistSensorState(SensorState state);
	
	public void connect(SensorState data1, SensorState data2, Classifier classifier1, Classifier classifier2);
	
	public List<SensorState> loadCurrentState();
	
	public List<Neighbour> loadNeighbours(SensorState start);
	
	public SensorState loadSensorState(String sensor);
	
	public void persistSensorConfig(SensorConfig sensorConfig);
	
	public SensorConfig loadSensorConfig();
	
	public void persistPanoptesConfig(PanoptesConfiguration panoptesConfig);
	
	public PanoptesConfiguration loadPanoptesConfig();
	
	public void persistPolicies(Class<? extends Policy> policyClass, List<Class<?>> policies);
	
	public PolicyConfiguration loadPolicies(Class<? extends Policy> policyClass);
	
	public String loadSelectedPolicy(Class<? extends Policy> policyClass);
	
	public List<ReportEvent> loadReports();
	
	public EventBus getEventBus();
	
	public void reset(boolean full);
}
