package at.ac.tuwien.infosys.api.events;

import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorChart;

public class ChartConfigEvent implements Event {

	private List<SensorChart> sensorCharts;

	public List<SensorChart> getSensorCharts() {
		return sensorCharts;
	}

	public void setSensorCharts(List<SensorChart> sensorCharts) {
		this.sensorCharts = sensorCharts;
	}
}
