package at.ac.tuwien.infosys.api.events;

import java.text.SimpleDateFormat;
import java.util.Date;

import at.ac.tuwien.infosys.api.policy.Policy;

public class ReportEvent implements Event {

	private Date date;
	private String report;

	private Class<? extends Policy> policy;

	public ReportEvent(Class<? extends Policy> policy) {
		this.policy = policy;
	}
	
	public Date getDate() {
		return date;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.date = new Date();
		this.report = report;
	}

	public Class<? extends Policy> getPolicy() {
		return policy;
	}

	public void setPolicy(Class<? extends Policy> policy) {
		this.policy = policy;
	}

	@Override
	public String toString() {
		SimpleDateFormat sf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		return sf.format(getDate()) + ": " + getReport();
	}

}