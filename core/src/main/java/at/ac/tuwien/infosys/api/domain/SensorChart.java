package at.ac.tuwien.infosys.api.domain;

public class SensorChart {

	private String sensor;
	private boolean view;
	
	public SensorChart(String sensor, boolean view) {
		this.sensor = sensor;
		this.view = view;
	}
	
	public String getSensor() {
		return sensor;
	}
	public void setSensor(String sensor) {
		this.sensor = sensor;
	}
	public boolean isView() {
		return view;
	}
	public void setView(boolean view) {
		this.view = view;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj instanceof String) {
			return ((String)obj).equals(sensor);
		}
		if (obj instanceof SensorChart) {
			return ((SensorChart)obj).getSensor().equals(sensor);
		}
		return false;
	}
}
