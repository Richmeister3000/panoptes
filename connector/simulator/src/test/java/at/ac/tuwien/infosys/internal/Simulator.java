package at.ac.tuwien.infosys.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Simulator {

	private static final int DEVIATION = 100;

	private Random rand;

	public Simulator() {
		rand = new Random(205);
	}

	public static void main(String... args) {
		Simulator simulator = new Simulator();
		try {
			simulator.simulate();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void simulate() throws IOException {
		BufferedReader reader2504 = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(
				"/1705")));
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File("solar")));
		String line = "";
		while ((line = reader2504.readLine()) != null) {

			double normal = Double.valueOf(line).doubleValue();

			// measures 10% less than normal
			double shadow = normal - (normal * 0.1);

			// better solar sensor measures 20% more than normal
			double better = normal + (normal * 0.2);

			// worser solar sensor measures 20% less than normal
			double worser = normal - (normal * 0.2);
			
			// measures 10% better than normal
			double betterShadow = normal + (normal * 0.1);
			
			// measures 30% worser than normal
			double worserShadow = normal - (normal * 0.1);

			// 10 x normal, shadow, better, worser
			String strLine = "";
			for (int i = 0; i < 4; i++) {
				if ("".equals(strLine))
					strLine = randomLine(normal, shadow, better, worser, worserShadow, betterShadow);
				else
					strLine += "," + randomLine(normal, shadow, better, worser, worserShadow, betterShadow);
			}
			writer.write(strLine + "\n");
		}
		writer.flush();
		writer.close();
		reader2504.close();
	}

	private String randomLine(double normal, double shadow, double better, double worser, double worserShadow, double betterShadow) {
		String line = "";
		line += Math.max(0, dev(normal));
		line += "," + Math.max(0, dev(shadow));
		line += "," + Math.max(0, dev(better));
		line += "," + Math.max(0, dev(worser));
		line += "," + Math.max(0, dev(betterShadow));
		line += "," + Math.max(0, dev(worserShadow));
		return line;
	}

	private long dev(double value) {
		return rand.nextBoolean() || value == 0 ? Math.max(0, Math.round((value - rand.nextInt(DEVIATION)))) : Math.round((value + rand.nextInt(DEVIATION)));
	}
}
