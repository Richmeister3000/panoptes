package at.ac.tuwien.infosys.internal;

import at.ac.tuwien.infosys.api.domain.SensorData;

public class SolarSensor {

	private SensorData sensorData;

	public double[] normal;

	public SolarSensor(String name, double[] normal) {
		sensorData = new SensorData();
		sensorData.setName(name);
		this.normal = normal;
	}

	public String getName() {
		return this.sensorData.getName();
	}

	public SensorData monitor(int idx) {
		double measurement = 0;
		measurement = normal[idx];
		this.sensorData.setMeasurement(measurement);
		return this.sensorData;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof String) {
			String name = (String) o;
			return name.equals(this.getName());
		}
		if (o instanceof SolarSensor) {
			SolarSensor sensor = (SolarSensor) o;
			return sensor.getName().equals(this.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}
}
