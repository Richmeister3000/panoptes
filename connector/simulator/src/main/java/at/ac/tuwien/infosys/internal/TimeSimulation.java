package at.ac.tuwien.infosys.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeSimulation implements Runnable {

	private static Thread thread;
	private static TimeSimulation timeSim;

	private int time;
	private int interval;
	private int duration;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TimeSimulation.class);

	public TimeSimulation(int interval, int duration) {
		this.interval = interval;
		this.duration = duration;
		this.time = 0;
	}

	public static TimeSimulation start(int interval, int duration) {
		timeSim = new TimeSimulation(interval, duration);
		thread = new Thread(timeSim);
		thread.start();
		return timeSim;

	}

	public static void stop() {
		thread.interrupt();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public int getTime() {
		return time;
	}
	
	public void resetTime() {
		time = 0;
	}

	public void run() {
		try {
			while (true) {
				Thread.sleep(this.duration);
				if (time == interval) {
					time = 0;
				} else {
					time++;
				}
			}
		} catch (InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}
}
