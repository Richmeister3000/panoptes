package at.ac.tuwien.infosys.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import at.ac.tuwien.infosys.api.AutonomicManager;
import at.ac.tuwien.infosys.api.ManagedElement;
import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.domain.SensorData;

public class SolarSensorPanel implements ManagedElement {

	private AutonomicManager autonomicManager;
	private BundleContext context;

	private static final Logger LOGGER = LoggerFactory.getLogger(SolarSensorPanel.class);
	private List<SolarSensor> sensors;

	private int numInstances = 0;
	private TimeSimulation clock;

	public SolarSensorPanel() {
		sensors = new ArrayList<SolarSensor>();
	}

	public void effect(SensorConfig config) {
		LOGGER.info("received new configuration!");
		
		if (config.isReset()) {
			clock.resetTime();
		}
		
		List<SensorData> sensorData = new ArrayList<SensorData>();
		for (String sensor : config.getSensors()) {
			SolarSensor solarSensor = this.loadSolarSensor(sensor);
			if (solarSensor != null) {
				sensorData.add(solarSensor.monitor(clock.getTime()));
			}
		}
		this.autonomicManager.manage(sensorData);
	}

	private SolarSensor loadSolarSensor(String name) {
		for (SolarSensor sensor : sensors) {
			if (sensor.getName().equals(name)) {
				return sensor;
			}
		}
		return null;
	}

	public void init() {
		try {
			InputStream arffStream = this.getClass().getResourceAsStream("/ARFF/Training27041705Dev100.arff");
			BufferedReader reader = new BufferedReader(new InputStreamReader(arffStream));
			ArffReader arff = new ArffReader(reader);

			Instances trainingSet = arff.getData();

			numInstances = trainingSet.numInstances();
			clock = TimeSimulation.start(numInstances - 1, 3000);

			for (int i = 0; i < trainingSet.numAttributes(); i++) {
				double[] normal = new double[trainingSet.numInstances()];
				for (int x = 0; x < numInstances; x++) {
					normal[x] = trainingSet.instance(x).value(i);
				}
				sensors.add(new SolarSensor(trainingSet.attribute(i).name(), normal));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		TimeSimulation.stop();
	}

	public AutonomicManager getAutonomicManager() {
		return autonomicManager;
	}

	public void setAutonomicManager(AutonomicManager autonomicManager) {
		this.autonomicManager = autonomicManager;
	}

	public BundleContext getContext() {
		return context;
	}

	public void setContext(BundleContext context) {
		this.context = context;
	}
}
