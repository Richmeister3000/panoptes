package at.ac.tuwien.infosys.internal;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.api.AutonomicManager;
import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorData;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.mapek.Analyze;
import at.ac.tuwien.infosys.api.mapek.Execute;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.mapek.Monitor;
import at.ac.tuwien.infosys.api.mapek.Plan;

public class MonitorEngine implements AutonomicManager, Runnable {

	private Monitor monitorService;
	private Analyze analyzeService;
	private Plan planService;
	private Execute executeService;
	private KnowledgeBase knowledgeBaseService;

	private static final Logger LOGGER = LoggerFactory.getLogger(MonitorEngine.class);
	private ScheduledExecutorService scheduledExecutorService;

	private HeartBeat heartBeat;
	private MonitoringChart monitoringChart;

	public void manage(List<SensorData> sensorData) {
		LOGGER.info("Received sensor data!");
		this.monitorService.collect(sensorData);
		this.analyzeService.analyze();

		for (SensorState state : this.knowledgeBaseService.loadCurrentState()) {
			monitoringChart.add(state.calculationMean(), "c_" + state.getSensor());
		}

		PanoptesConfiguration config = this.knowledgeBaseService.loadPanoptesConfig();
		if (config.isAccurateDeviation()) {
			monitoringChart.add(config.getDeviation(), "error");
		}
		monitoringChart.add(config.getMaxError(), "maxError");

		for (SensorData data : sensorData) {
			monitoringChart.add(data.getMeasurement(), "m_" + data.getName());
		}
	}

	public void init() {
		LOGGER.info("Initializing asynchrone Autonomic Manager!");
		scheduledExecutorService = Executors.newScheduledThreadPool(1);
		scheduledExecutorService.scheduleAtFixedRate(heartBeat, 0, 1, TimeUnit.SECONDS);
	}

	public void destroy() {
		scheduledExecutorService.shutdown();
		heartBeat.destroy();
	}

	public void run() {
		this.planService.decide();
		this.executeService.act();
	}

	public Monitor getMonitorService() {
		return monitorService;
	}

	public void setMonitorService(Monitor monitor) {
		this.monitorService = monitor;
	}

	public Analyze getAnalyzeService() {
		return analyzeService;
	}

	public void setAnalyzeService(Analyze analyze) {
		this.analyzeService = analyze;
	}

	public Plan getPlanService() {
		return planService;
	}

	public void setPlanService(Plan plan) {
		this.planService = plan;
	}

	public Execute getExecuteService() {
		return executeService;
	}

	public void setExecuteService(Execute execute) {
		this.executeService = execute;
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBase) {
		this.knowledgeBaseService = knowledgeBase;
	}

	public HeartBeat getHeartBeat() {
		return heartBeat;
	}

	public void setHeartBeat(HeartBeat heartBeat) {
		this.heartBeat = heartBeat;
	}

	public MonitoringChart getMonitoringChart() {
		return monitoringChart;
	}

	public void setMonitoringChart(MonitoringChart monitoringChart) {
		this.monitoringChart = monitoringChart;
	}
}
