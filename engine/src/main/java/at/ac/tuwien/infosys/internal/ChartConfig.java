package at.ac.tuwien.infosys.internal;

import org.jfree.data.time.TimeSeries;

public class ChartConfig {

	private String id;
	private TimeSeries series;
	private boolean view;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public TimeSeries getSeries() {
		return series;
	}
	public void setSeries(TimeSeries series) {
		this.series = series;
	}
	public boolean isView() {
		return view;
	}
	public void setView(boolean view) {
		this.view = view;
	}
}
