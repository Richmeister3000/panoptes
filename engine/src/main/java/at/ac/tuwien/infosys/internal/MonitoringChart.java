package at.ac.tuwien.infosys.internal;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import at.ac.tuwien.infosys.api.domain.SensorChart;
import at.ac.tuwien.infosys.api.events.ChartConfigEvent;
import at.ac.tuwien.infosys.api.events.ChartEvent;
import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class MonitoringChart implements Subscriber<Event> {

	private TimeSeriesCollection seriesCollection;
	private final JFreeChart chart;

	private List<ChartConfig> chartConfig;

	private KnowledgeBase knowledgeBaseService;

	public MonitoringChart() {
		seriesCollection = new TimeSeriesCollection();
		chart = ChartFactory.createTimeSeriesChart("Panoptes", "Time", "Calculation", seriesCollection, true, true,
				false);
		chartConfig = new ArrayList<ChartConfig>();
	}

	public void init() {
		// subscription for events
		knowledgeBaseService.getEventBus().subscribe(Event.class, this);
	}

	@Override
	public void notify(Event event) {
		if (event instanceof ChartConfigEvent) {
			ChartConfigEvent chartConfigEvent = (ChartConfigEvent) event;
			seriesCollection.removeAllSeries();
			for (ChartConfig config : chartConfig) {
				boolean view = false;
				for (SensorChart sensorChart : chartConfigEvent.getSensorCharts()) {
					if (sensorChart.getSensor().equals(config.getId())) {
						view = true;
						break;
					}
				}

				config.setView(view);
				if (view) {
					seriesCollection.addSeries(config.getSeries());
				}
			}
			this.publishChart();
		}
		if (event instanceof PlainEvent) {
			PlainEvent plainEvent = (PlainEvent) event;
			if ("ResetEvent".equals(plainEvent.getEvent())) {
				chartConfig.clear();
			}
			
		}
	}

	private ChartConfig find(String id) {
		for (ChartConfig config : chartConfig) {
			if (config.getId().equals(id)) {
				return config;
			}
		}
		return null;
	}

	public void add(double value, String id) {

		ChartConfig config = null;
		if ((config = this.find(id)) == null) {
			config = new ChartConfig();
			config.setId(id);
			config.setView(false);
			config.setSeries(new TimeSeries(id));
			chartConfig.add(config);
		}
		Calendar cal = Calendar.getInstance();

		config.getSeries().addOrUpdate(
				new Second(cal.get(Calendar.SECOND), cal.get(Calendar.MINUTE), cal.get(Calendar.HOUR_OF_DAY),
						cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)), value);

		this.publishChart();
		// this.render();
	}

	private void publishChart() {
		ChartEvent event = new ChartEvent();
		event.setChart(chart);
		knowledgeBaseService.getEventBus().publish(event);
	}

	public void render() {
		BufferedImage bufferedImage = chart.createBufferedImage(1000, 600);
		ByteArrayOutputStream bas = new ByteArrayOutputStream();
		try {
			ImageIO.write(bufferedImage, "png", bas);
		} catch (IOException e) {
			e.printStackTrace();
		}

		byte[] byteArray = bas.toByteArray();

		try {
			InputStream in = new ByteArrayInputStream(byteArray);
			BufferedImage image = ImageIO.read(in);
			File outputfile = new File("panoptes.png");
			ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBase) {
		this.knowledgeBaseService = knowledgeBase;
	}
}
