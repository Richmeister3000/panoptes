package at.ac.tuwien.infosys.internal;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class HeartBeat implements Runnable, Subscriber<PlainEvent> {

	private Runnable autonomicManager;
	private KnowledgeBase knowledgeBaseService;

	private ScheduledExecutorService scheduledExecutorService;
	private ScheduledFuture<?> scheduledFuture;

	private long freq;

	private boolean trained;

	public HeartBeat(Runnable autonomicManager) {
		this.autonomicManager = autonomicManager;
		scheduledExecutorService = Executors.newScheduledThreadPool(1);
		freq = 1;
		trained = false;
	}

	public void init() {
		knowledgeBaseService.getEventBus().subscribe(PlainEvent.class, this);
	}

	private void start() {
		if (scheduledFuture == null && trained) {
			long frequency = knowledgeBaseService.loadPanoptesConfig().getFrequency();
			scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(autonomicManager, 0, frequency,
					TimeUnit.SECONDS);
		}
	}

	private void stop() {
		if (scheduledFuture != null) {
			scheduledFuture.cancel(false);
			scheduledFuture = null;
		}
	}

	public void run() {
		if (trained) {
			long frequency = knowledgeBaseService.loadPanoptesConfig().getFrequency();
			if (freq != frequency) {
				scheduledFuture.cancel(false);
				if (scheduledFuture.isDone()) {
					scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(autonomicManager, frequency,
							frequency, TimeUnit.SECONDS);
					freq = frequency;
				}
			}
		}
	}

	public void destroy() {
		scheduledExecutorService.shutdown();
		knowledgeBaseService.getEventBus().unsubscribe(PlainEvent.class, this);
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBaseService) {
		this.knowledgeBaseService = knowledgeBaseService;
	}

	@Override
	public void notify(PlainEvent event) {
		if ("TrainEvent".equals(event.getEvent())) {
			trained = false;
			this.stop();
		}
		if ("FinishedTrainEvent".equals(event.getEvent())) {
			trained = true;
			this.start();
		}
		if ("StopEvent".equals(event.getEvent())) {
			this.stop();
		}
		if ("StartEvent".equals(event.getEvent())) {
			this.start();
		}
	}
}
