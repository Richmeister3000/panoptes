package at.ac.tuwien.infosys;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.jfree.data.xy.XYDataset;
import org.junit.Test;

public class JFreeChartTest {

	@Test
	public void barCharttest() throws Exception {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.setValue(2, "Marks", "Rahul");
		dataset.setValue(7, "Marks", "Vinod");
		dataset.setValue(4, "Marks", "Deepak");
		dataset.setValue(9, "Marks", "Prashant");
		dataset.setValue(6, "Marks", "Chandan");
		final JFreeChart chart = ChartFactory.createBarChart("BarChart", "Student", "Marks", dataset,
				PlotOrientation.VERTICAL, false, true, false);
		
		this.render(chart);
	}

	@Test
	public void normalDistributionRenderImagetest() throws Exception {
		Function2D normal = new NormalDistributionFunction2D(0.0, 1.0);
		XYDataset dataset = DatasetUtilities.sampleFunction2D(normal, -5.0, 5.0, 100, "Normal");
		final JFreeChart chart = ChartFactory.createXYLineChart("XY Series Demo", "X", "Y", dataset,
				PlotOrientation.VERTICAL, true, true, false);
		this.render(chart);
	}

	@Test
	public void timeSeriestest() throws Exception {
		TimeSeriesCollection seriesCollection = new TimeSeriesCollection();
		TimeSeries series1 = new TimeSeries("series1");
		series1.add(new TimeSeriesDataItem(new Second(0, 51, 20, 06, 06, 2012), 4.0));
		seriesCollection.addSeries(series1);
		TimeSeries series2 = new TimeSeries("series2");
		series2.add(new TimeSeriesDataItem(new Second(0, 51, 20, 06, 06, 2012), 6.0));
		seriesCollection.addSeries(series2);

		final JFreeChart chart = ChartFactory.createTimeSeriesChart("Time Series Test", "X", "Y", seriesCollection,
				true, true, false);

		series1.add(new TimeSeriesDataItem(new Second(0, 52, 20, 06, 06, 2012), 87.0));
		series2.add(new TimeSeriesDataItem(new Second(0, 52, 20, 06, 06, 2012), 89.0));

		this.render(chart);
	}
	
	private void render(JFreeChart chart) throws IOException {
		BufferedImage bufferedImage = chart.createBufferedImage(600, 800);
		ByteArrayOutputStream bas = new ByteArrayOutputStream();
		try {
			ImageIO.write(bufferedImage, "png", bas);
		} catch (IOException e) {
			e.printStackTrace();
		}

		byte[] byteArray = bas.toByteArray();

		InputStream in = new ByteArrayInputStream(byteArray);
		BufferedImage image = ImageIO.read(in);
		File outputfile = new File("image.png");
		ImageIO.write(image, "png", outputfile);
	}
}
