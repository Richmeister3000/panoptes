package at.ac.tuwien.infosys;

import org.junit.Ignore;
import org.junit.Test;

import at.ac.tuwien.infosys.internal.MonitoringChart;

public class MonitoringChartTest {

	@Ignore
	@Test
	public void addRendertest() throws Exception {
		MonitoringChart mChart = new MonitoringChart();
		mChart.add(4.0, "sensor1");
		mChart.add(14.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(5.0, "sensor1");
		mChart.add(15.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(6.0, "sensor1");
		mChart.add(16.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(7.0, "sensor1");
		mChart.add(17.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(6.0, "sensor1");
		mChart.add(16.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(5.0, "sensor1");
		mChart.add(15.2, "sensor2");
		Thread.sleep(1000);
		mChart.add(4.0, "sensor1");
		mChart.add(14.2, "sensor2");
		
		mChart.render();
	}
}
