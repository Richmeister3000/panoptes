package at.ac.tuwien.infosys.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Simulator {

	private static final int DEVIATION = 150;

	private Random rand;

	public Simulator() {
		rand = new Random(205);
	}

	public static void main(String... args) {
		Simulator simulator = new Simulator();
		try {
			simulator.simulate();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void simulate() throws IOException {
		BufferedReader reader2504 = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(
				"/2504")));
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File("solar")));
		String line25 = "";
		while ((line25 = reader2504.readLine()) != null) {

			double normal = Double.valueOf(line25).doubleValue();

			// measures 10% less than normal
			double shadow = normal - (normal * 0.1);

			// better solar sensor measures 20% more than normal
			double better = normal + (normal * 0.2);

			// worser solar sensor measures 20% less than normal
			double worser = normal - (normal * 0.2);

			// 10 x normal, shadow, better, worser
			String line = "";
			for (int i = 0; i < 5; i++) {
				if ("".equals(line))
					line = randomLine(normal, shadow, better, worser);
				else
					line += "," + randomLine(normal, shadow, better, worser);
			}
			writer.write(line + "\n");
		}
		writer.flush();
		writer.close();
		reader2504.close();
	}

	private String randomLine(double normal, double shadow, double better, double worser) {
		String line = "";
		line += Math.max(0, dev(normal));
		line += "," + Math.max(0, dev(shadow));
		line += "," + Math.max(0, dev(better));
		line += "," + Math.max(0, dev(worser));
		return line;
	}

	private long dev(double value) {
		return rand.nextBoolean() || value == 0 ? Math.max(0, Math.round((value - rand.nextInt(DEVIATION)))) : Math.round((value + rand.nextInt(DEVIATION)));
	}
}
