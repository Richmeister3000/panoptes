package at.ac.tuwien.infosys.internal.policies;

import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.AdaptationPolicy;

public class IncrementalAdaptation extends AdaptationPolicy {
	
	private KnowledgeBase knowledgeBase;

	private int counter;
	
	public IncrementalAdaptation(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
		counter = 0;
	}
	
	public void adapt() {
		List<SensorState> vertices = knowledgeBase.loadCurrentState();
		int sampleSize = knowledgeBase.loadPanoptesConfig().getSampleSize();
		
		SensorConfig config = new SensorConfig();
		if (vertices.size() > 0) {
			for (int i = 0; i < sampleSize; i++) {
				SensorState state = vertices.get(counter);
				config.addSensor(state.getSensor());
				if (counter == vertices.size() - 1) {
					counter = 0;
				} else {
					counter++;
				}
			}
			knowledgeBase.persistSensorConfig(config);
		}
	}
}
