package at.ac.tuwien.infosys.internal.policies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.ChangeEvent;
import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;

public class QuickSortFrequency extends FrequencyPolicy implements Subscriber<Event> {

	private Map<SensorState, Long> frequencies;
	private KnowledgeBase knowledgeBase;

	private static final long MAX_FREQUENCY = 200;
	private static final long MIN_FREQUENCY = 1;

	public QuickSortFrequency(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
		// register for events
		frequencies = new HashMap<SensorState, Long>();
	}

	@Override
	public void init() {
		knowledgeBase.getEventBus().subscribe(ChangeEvent.class, this);
	}

	@Override
	public void destroy() {
		knowledgeBase.getEventBus().unsubscribe(ChangeEvent.class, this);
	}

	public void frequency() {
		long frequency = 0;
		List<SensorState> vertices = knowledgeBase.loadCurrentState();
		for (Long freq : frequencies.values()) {
			frequency += freq;
		}
		frequency = Math.max(MIN_FREQUENCY,
				Math.min(MAX_FREQUENCY, vertices.size() > 0 ? frequency / vertices.size() : MIN_FREQUENCY));
		PanoptesConfiguration config = knowledgeBase.loadPanoptesConfig();
		if (config.getFrequency() != frequency) {
			config.setFrequency(frequency);
			knowledgeBase.persistPanoptesConfig(config);

			ReportEvent event = new ReportEvent(FrequencyPolicy.class);
			event.setReport("Changed frequency to " + frequency + ".");
			knowledgeBase.getEventBus().publish(event);
		}
	}

	public void notify(Event event) {
		if (event instanceof ChangeEvent) {
			ChangeEvent changeEvent = (ChangeEvent) event;
			boolean raise = true;
			if (changeEvent.getOldMeasurement() >= 0) {
				double failure = changeEvent.getError();
				raise = (changeEvent.getNewMeasurement() <= (changeEvent.getOldMeasurement() + failure) && changeEvent
						.getNewMeasurement() >= (changeEvent.getOldMeasurement() - failure));
			}
			long frequency = MIN_FREQUENCY;
			if (frequencies.containsKey(changeEvent.getSource())) {
				frequency = frequencies.get(changeEvent.getSource());
			}
			frequency = (raise ? frequency * 2 : frequency > 1 ? frequency / 2 : frequency);
			frequencies.put(changeEvent.getSource(), frequency);
		}
		if (event instanceof PlainEvent) {
			if ("ResetEvent".equals(((PlainEvent) event).getEvent())) {
				frequencies.clear();
			}
		}
	}
}