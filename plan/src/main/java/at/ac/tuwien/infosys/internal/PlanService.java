package at.ac.tuwien.infosys.internal;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.mapek.Plan;
import at.ac.tuwien.infosys.api.policy.AdaptationPolicy;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;
import at.ac.tuwien.infosys.internal.policies.ContinuousSampleSize;
import at.ac.tuwien.infosys.internal.policies.FullFrequency;
import at.ac.tuwien.infosys.internal.policies.FullSampleSize;
import at.ac.tuwien.infosys.internal.policies.IncrementalAdaptation;
import at.ac.tuwien.infosys.internal.policies.JumpingSampleSize;
import at.ac.tuwien.infosys.internal.policies.LinearDeviationFrequency;
import at.ac.tuwien.infosys.internal.policies.QuickSortFrequency;
import at.ac.tuwien.infosys.internal.policies.RandomAdaptation;
import at.ac.tuwien.infosys.internal.policies.ReactionFrequency;
import at.ac.tuwien.infosys.internal.policies.ResetSampleSize;

public class PlanService implements Plan {

	private KnowledgeBase knowledgeBaseService;

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanService.class);

	public void init() {
		PolicyFactory.init(knowledgeBaseService);
		try {
			knowledgeBaseService.persistPolicies(
					FrequencyPolicy.class,
					Arrays.asList(new Class<?>[] { QuickSortFrequency.class, ReactionFrequency.class,
							FullFrequency.class, LinearDeviationFrequency.class }));
			knowledgeBaseService.persistPolicies(
					SampleSizePolicy.class,
					Arrays.asList(new Class<?>[] { ResetSampleSize.class, ContinuousSampleSize.class,
							JumpingSampleSize.class, FullSampleSize.class }));
			knowledgeBaseService.persistPolicies(AdaptationPolicy.class,
					Arrays.asList(new Class<?>[] { IncrementalAdaptation.class, RandomAdaptation.class }));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public void destroy() {
		PolicyFactory.destroy();
	}

	public void decide() {
		try {
			PolicyFactory.getSampleSizePolicy().sample();
			PolicyFactory.getFrequencyPolicy().frequency();
			PolicyFactory.getAdaptationPolicy().adapt();
		} catch (ClassNotFoundException e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBase) {
		this.knowledgeBaseService = knowledgeBase;
	}
}
