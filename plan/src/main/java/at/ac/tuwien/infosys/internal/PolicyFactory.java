package at.ac.tuwien.infosys.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.AdaptationPolicy;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;
import at.ac.tuwien.infosys.api.policy.Policy;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;

public class PolicyFactory implements Subscriber<PlainEvent> {

	private SampleSizePolicy sampleSizePolicy;
	private FrequencyPolicy frequencyPolicy;
	private AdaptationPolicy adaptationPolicy;

	private KnowledgeBase knowledgeBase;

	private static PolicyFactory instance;

	private static final String packageName = PlanService.class.getPackage().getName() + "." + "policies.";

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyFactory.class);

	private static PolicyFactory getInstance() {
		if (instance == null) {
			instance = new PolicyFactory();
		}
		return instance;
	}

	public static void init(KnowledgeBase knowledgeBase) {
		getInstance().knowledgeBase = knowledgeBase;
		getInstance().knowledgeBase.getEventBus().subscribe(PlainEvent.class, getInstance());
	}

	public static void destroy() {
		getInstance().knowledgeBase.getEventBus().unsubscribe(PlainEvent.class, getInstance());
	}

	private static Class<?> loadSelectedPolicy(Class<? extends Policy> clazz) throws ClassNotFoundException {
		return Class.forName(packageName + getInstance().knowledgeBase.loadSelectedPolicy(clazz));
	}

	@SuppressWarnings("unchecked")
	public static AdaptationPolicy getAdaptationPolicy() throws ClassNotFoundException {
		if (getInstance().adaptationPolicy == null) {
			Class<?> policyClass = loadSelectedPolicy(AdaptationPolicy.class);
			if (AdaptationPolicy.class.isAssignableFrom(policyClass)) {
				createAdaptationPolicy((Class<? extends AdaptationPolicy>) policyClass);
			}
		}
		return getInstance().adaptationPolicy;
	}

	@SuppressWarnings("unchecked")
	public static SampleSizePolicy getSampleSizePolicy() throws ClassNotFoundException {
		if (getInstance().sampleSizePolicy == null) {
			Class<?> policyClass = loadSelectedPolicy(SampleSizePolicy.class);
			if (SampleSizePolicy.class.isAssignableFrom(policyClass)) {
				createSampleSizePolicy((Class<? extends SampleSizePolicy>) policyClass);
			}
		}
		return getInstance().sampleSizePolicy;
	}

	@SuppressWarnings("unchecked")
	public static FrequencyPolicy getFrequencyPolicy() throws ClassNotFoundException {
		if (getInstance().frequencyPolicy == null) {
			Class<?> policyClass = loadSelectedPolicy(FrequencyPolicy.class);
			if (FrequencyPolicy.class.isAssignableFrom(policyClass)) {
				createFrequencyPolicy((Class<? extends FrequencyPolicy>) policyClass);
			}
		}
		return getInstance().frequencyPolicy;
	}

	public static SampleSizePolicy createSampleSizePolicy(Class<? extends SampleSizePolicy> clazz)
			throws ClassNotFoundException {
		if (getInstance().sampleSizePolicy != null) {
			getInstance().sampleSizePolicy.destroy();
		}
		getInstance().sampleSizePolicy = getInstance().create(clazz);
		getInstance().sampleSizePolicy.init();
		return getSampleSizePolicy();
	}

	public static FrequencyPolicy createFrequencyPolicy(Class<? extends FrequencyPolicy> clazz)
			throws ClassNotFoundException {
		if (getInstance().frequencyPolicy != null) {
			getInstance().frequencyPolicy.destroy();
		}
		getInstance().frequencyPolicy = getInstance().create(clazz);
		getInstance().frequencyPolicy.init();
		return getFrequencyPolicy();
	}

	public static AdaptationPolicy createAdaptationPolicy(Class<? extends AdaptationPolicy> clazz)
			throws ClassNotFoundException {
		if (getInstance().adaptationPolicy != null) {
			getInstance().adaptationPolicy.destroy();
		}
		getInstance().adaptationPolicy = getInstance().create(clazz);
		getInstance().adaptationPolicy.init();
		return getAdaptationPolicy();
	}

	private <T> T create(Class<T> clazz) {
		if (clazz == null)
			return null;
		try {
			return clazz.getConstructor(KnowledgeBase.class).newInstance(knowledgeBase);
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void notify(PlainEvent event) {
		if ("PolicyEvent".equals(event.getEvent())) {
			try {
				Class<?> frequencyPolicy = loadSelectedPolicy(FrequencyPolicy.class);
				if (FrequencyPolicy.class.isAssignableFrom(frequencyPolicy)) {
					createFrequencyPolicy((Class<? extends FrequencyPolicy>) frequencyPolicy);
				}
				Class<?> sampleSizePolicy = loadSelectedPolicy(SampleSizePolicy.class);
				if (SampleSizePolicy.class.isAssignableFrom(sampleSizePolicy)) {
					createSampleSizePolicy((Class<? extends SampleSizePolicy>) sampleSizePolicy);
				}
				Class<?> adaptationPolicy = loadSelectedPolicy(AdaptationPolicy.class);
				if (AdaptationPolicy.class.isAssignableFrom(adaptationPolicy)) {
					createAdaptationPolicy((Class<? extends AdaptationPolicy>) adaptationPolicy);
				}
			} catch (ClassNotFoundException e) {
				LOGGER.error(e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
