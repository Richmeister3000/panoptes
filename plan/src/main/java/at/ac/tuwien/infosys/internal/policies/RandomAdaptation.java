package at.ac.tuwien.infosys.internal.policies;

import java.util.Collections;
import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.AdaptationPolicy;

public class RandomAdaptation extends AdaptationPolicy {
	
	private KnowledgeBase knowledgeBase;
	
	public RandomAdaptation(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}
	
	public void adapt() {
		List<SensorState> vertices = knowledgeBase.loadCurrentState();
		int sampleSize = knowledgeBase.loadPanoptesConfig().getSampleSize();
		
		SensorConfig config = new SensorConfig();
		if (vertices.size() > 0) {
			Collections.shuffle(vertices);
			for (int i = 0; i < sampleSize; i++) {
				SensorState state = vertices.get(i);
				config.addSensor(state.getSensor());
			}
			knowledgeBase.persistSensorConfig(config);
		}
	}
}
