package at.ac.tuwien.infosys.internal.policies;

import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;

public class FullSampleSize extends SampleSizePolicy {

	private KnowledgeBase knowledgeBase;

	public FullSampleSize(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}

	public void sample() {
		int sampleSize = knowledgeBase.loadCurrentState().size();
		PanoptesConfiguration panoptesConfig = knowledgeBase.loadPanoptesConfig();
		panoptesConfig.setAccurateDeviation(true);
		if (panoptesConfig.getSampleSize() != sampleSize) {
			panoptesConfig.setSampleSize(sampleSize);
			knowledgeBase.persistPanoptesConfig(panoptesConfig);
			
			ReportEvent event = new ReportEvent(SampleSizePolicy.class);
			event.setReport("Changed sample size to " + sampleSize + ".");
			knowledgeBase.getEventBus().publish(event);
		}
	}
}
