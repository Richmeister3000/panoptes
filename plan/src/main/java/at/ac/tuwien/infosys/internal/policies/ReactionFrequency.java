package at.ac.tuwien.infosys.internal.policies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.ChangeEvent;
import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;

public class ReactionFrequency extends FrequencyPolicy implements Subscriber<Event> {

	private Map<SensorState, Long> frequencies;
	private KnowledgeBase knowledgeBase;

	private long currentFrequency;

	private static final int MAX_FREQUENCY = 200;
	private static final int MIN_FREQUENCY = 1;

	boolean sampleSizeChange;

	public ReactionFrequency(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
		frequencies = new HashMap<SensorState, Long>();
		sampleSizeChange = false;
		currentFrequency = MIN_FREQUENCY;
	}

	@Override
	public void init() {
		knowledgeBase.getEventBus().subscribe(Event.class, this);
	}

	@Override
	public void destroy() {
		knowledgeBase.getEventBus().unsubscribe(Event.class, this);
	}

	public void frequency() {
		long frequency = 0;
		List<SensorState> vertices = knowledgeBase.loadCurrentState();
		for (Long freq : frequencies.values()) {
			frequency += freq;
		}

		PanoptesConfiguration config = this.knowledgeBase.loadPanoptesConfig();
		if (currentFrequency > frequency || (!sampleSizeChange && config.getSampleSize() != this.knowledgeBase.loadCurrentState().size())) {
			frequency = Math.max(MIN_FREQUENCY,
					Math.min(MAX_FREQUENCY, vertices.size() > 0 ? frequency / vertices.size() : MIN_FREQUENCY));
			config = knowledgeBase.loadPanoptesConfig();
			if (config.getFrequency() != frequency) {
				config.setFrequency(frequency);
				knowledgeBase.persistPanoptesConfig(config);

				ReportEvent event = new ReportEvent(FrequencyPolicy.class);
				event.setReport("Changed frequency to " + frequency + ".");
				knowledgeBase.getEventBus().publish(event);
			}
		}
	}

	@Override
	public void notify(Event event) {
		sampleSizeChange = false;
		if (event instanceof ChangeEvent) {
			ChangeEvent changeEvent = (ChangeEvent) event;
			boolean change = true;
			if (changeEvent.getOldMeasurement() >= 0) {
				double failure = changeEvent.getError();
				change = (changeEvent.getNewMeasurement() <= (changeEvent.getOldMeasurement() + failure) && changeEvent
						.getNewMeasurement() >= (changeEvent.getOldMeasurement() - failure));
			}
			long frequency = 1L;
			if (frequencies.containsKey(changeEvent.getSource())) {
				frequency = frequencies.get(changeEvent.getSource());
			}
			frequency = (change ? frequency * 2 : MIN_FREQUENCY);
			frequencies.put(changeEvent.getSource(), frequency);
		}

		if (event instanceof PlainEvent) {
			if ("ResetEvent".equals(((PlainEvent) event).getEvent())) {
				frequencies.clear();
			}
		}
		if (event instanceof ReportEvent) {
			if (SampleSizePolicy.class.equals(((ReportEvent) event).getPolicy())) {
				sampleSizeChange = true;
			}
		}
	}

}
