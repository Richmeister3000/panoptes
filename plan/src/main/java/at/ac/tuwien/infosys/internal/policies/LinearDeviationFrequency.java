package at.ac.tuwien.infosys.internal.policies;

import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;

public class LinearDeviationFrequency extends FrequencyPolicy implements Subscriber<Event> {

	private KnowledgeBase knowledgeBase;

	private static final int MIN_FREQUENCY = 1;

	private static final int COLLECTION = 25;
	private int counter;

	public LinearDeviationFrequency(KnowledgeBase knowledgeBase) {
		this(knowledgeBase, false);
	}

	public LinearDeviationFrequency(KnowledgeBase knowledgeBase, boolean test) {
		this.knowledgeBase = knowledgeBase;
		this.knowledgeBase.getEventBus().subscribe(ReportEvent.class, this);
		counter = test ? COLLECTION : 0;
	}

	@Override
	public void init() {
		knowledgeBase.getEventBus().subscribe(ReportEvent.class, this);
	}
	
	@Override
	public void destroy() {
		knowledgeBase.getEventBus().unsubscribe(ReportEvent.class, this);
	}
	
	public void frequency() {
		PanoptesConfiguration config = knowledgeBase.loadPanoptesConfig();
		long frequency = MIN_FREQUENCY;
		switch (counter) {
		case 0:
			config.setFrequency(frequency);
			knowledgeBase.persistPanoptesConfig(config);
			counter++;
			break;
		case COLLECTION:
			double deviation = config.getDeviation();
			double maxError = config.getMaxError();

			if (maxError >= deviation) {
				if (deviation > 0) {
					frequency = Math.round(Math.floor(maxError / deviation));
				} else {
					frequency = Math.round(Math.floor(maxError));
				}
			}

			if (config.getFrequency() != frequency) {
				config.setFrequency(frequency);
				knowledgeBase.persistPanoptesConfig(config);

				ReportEvent event = new ReportEvent(FrequencyPolicy.class);
				event.setReport("Changed frequency to " + frequency + ".");
				knowledgeBase.getEventBus().publish(event);
			}
			break;
		default:
			counter++;
		}
	}

	@Override
	public void notify(Event event) {
		if (event instanceof ReportEvent) {
			if (SampleSizePolicy.class.equals(((ReportEvent)event).getPolicy())) {
				counter = 0;
			}
		}
		if (event instanceof PlainEvent) {
			if ("Reset".equals(((PlainEvent) event).getEvent())) {
				counter = 0;
			}
		}
	}
}
