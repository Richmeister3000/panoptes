package at.ac.tuwien.infosys.internal.policies;

import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;

public class FullFrequency extends FrequencyPolicy {

	private KnowledgeBase knowledgeBase;

	public FullFrequency(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}

	@Override
	public void frequency() {
		PanoptesConfiguration config = knowledgeBase.loadPanoptesConfig();
		if (config.getFrequency() != 1L) {
			config.setFrequency(1L);
			knowledgeBase.persistPanoptesConfig(config);
			
			ReportEvent event = new ReportEvent(FrequencyPolicy.class);
			event.setReport("Changed frequency to " + 1 + ".");
			knowledgeBase.getEventBus().publish(event);
		}
	}
}
