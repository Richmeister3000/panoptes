package at.ac.tuwien.infosys.internal;

import org.junit.Test;
import org.mockito.Mockito;

import at.ac.tuwien.infosys.api.EventBus;
import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.internal.policies.LinearDeviationFrequency;

public class LinearDeviationFrequencyTest {
	
	private KnowledgeBase knowledgeBaseMock = Mockito.mock(KnowledgeBase.class);
	private PanoptesConfiguration panoptesConfigMock = Mockito.mock(PanoptesConfiguration.class);
	private EventBus eventBusMock = Mockito.mock(EventBus.class);
	
	@Test
	public void zerodeviatonfrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(0.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(3L);

		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(10L));
	}
	
	@Test
	public void deviatonfrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(1.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(1L);

		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(10L));
	}
	
	@Test
	public void lowfrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(2.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(3L);

		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(5L));
	}
	
	@Test
	public void middlefrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(5.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(1L);

		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(2L));
	}
	
	@Test
	public void highfrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(24L);

		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(1L));
	}
	
	@Test
	public void exceededfrequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(30.0);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(10.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(30L);
		
		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(1L));
	}
	
	@Test
	public void frequencytest() throws Exception {
		Mockito.when(knowledgeBaseMock.loadPanoptesConfig()).thenReturn(panoptesConfigMock);
		Mockito.when(panoptesConfigMock.getDeviation()).thenReturn(0.08);
		Mockito.when(panoptesConfigMock.getMaxError()).thenReturn(0.0);
		Mockito.when(panoptesConfigMock.getFrequency()).thenReturn(0L);
		
		Mockito.when(knowledgeBaseMock.getEventBus()).thenReturn(eventBusMock);

		LinearDeviationFrequency instance = new LinearDeviationFrequency(knowledgeBaseMock, true);
		instance.frequency();
		
		Mockito.verify(panoptesConfigMock).setFrequency(Mockito.eq(1L));
	}	
}
