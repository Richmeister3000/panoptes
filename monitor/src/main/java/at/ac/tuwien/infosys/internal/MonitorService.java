package at.ac.tuwien.infosys.internal;

import java.util.Date;
import java.util.List;

import at.ac.tuwien.infosys.api.domain.SensorData;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.ChangeEvent;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.mapek.Monitor;

public class MonitorService implements Monitor {

	private KnowledgeBase knowledgeBaseService;

	public void collect(List<SensorData> sensorData) {
		this.knowledgeBaseService.persistSnapshot(sensorData);
		long time = new Date().getTime();
		//clear last image
		for (SensorState state:this.knowledgeBaseService.loadCurrentState()) {
			state.resetCalculation();
		}
		for (SensorData data:sensorData) {
			SensorState state = this.knowledgeBaseService.loadSensorState(data.getName());
			if (state != null) {
				
				ChangeEvent event = new ChangeEvent();
				event.setOldMeasurement(state.getLastMeasurement());
				event.setNewMeasurement(data.getMeasurement());
				event.setError(state.getError());
				event.setSource(state);
				knowledgeBaseService.getEventBus().publish(event);
				
				state.setLastMeasured(time);
				state.setLastMeasurement(data.getMeasurement());
			}
			this.knowledgeBaseService.persistSensorState(state);
		}
		this.knowledgeBaseService.loadPanoptesConfig().addMeasurements(sensorData.size());
	}

	public KnowledgeBase getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBase knowledgeBase) {
		this.knowledgeBaseService = knowledgeBase;
	}
}
