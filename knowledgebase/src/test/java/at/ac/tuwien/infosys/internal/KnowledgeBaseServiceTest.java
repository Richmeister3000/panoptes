package at.ac.tuwien.infosys.internal;

import junit.framework.Assert;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;
import org.mockito.Mockito;

import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.internal.edge.ClassifierEdge;

public class KnowledgeBaseServiceTest {

	@Test
	public void simplejgraphtest() throws Exception {
		UndirectedGraph<String, DefaultEdge> graph = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
		String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";

        // add the vertices
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);

        // add edges to create a circuit
        graph.addEdge(v1, v2);
        graph.addEdge(v2, v3);
        graph.addEdge(v3, v4);
        graph.addEdge(v4, v1);
        
        DefaultEdge edge = graph.getEdge("v1", "v2");
        Assert.assertNotNull(edge);
        
        DefaultEdge edge1 = graph.getEdge("v2", "v1");
        Assert.assertNotNull(edge1);
        
        DefaultEdge edge2 = graph.getEdge("v2", "v4");
        Assert.assertNull(edge2);
	}
	
	@Test
	public void jgraphtest() throws Exception {
		UndirectedGraph<SensorState, ClassifierEdge> graph = new SimpleGraph<SensorState, ClassifierEdge>(ClassifierEdge.class);
		
		SensorState state1 = Mockito.mock(SensorState.class);
		SensorState state2 = Mockito.mock(SensorState.class);
		ClassifierEdge edge = Mockito.mock(ClassifierEdge.class);
		
		Assert.assertTrue(graph.addVertex(state1));
		Assert.assertTrue(graph.addVertex(state2));
		
		boolean value = graph.addEdge(state1, state2, edge);
		Assert.assertTrue(value);
		
		ClassifierEdge edge1 = graph.getEdge(state1, state2);
		Assert.assertEquals(edge, edge1);
	}
}
