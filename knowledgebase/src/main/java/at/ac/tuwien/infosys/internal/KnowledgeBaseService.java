package at.ac.tuwien.infosys.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;

import weka.classifiers.Classifier;
import at.ac.tuwien.infosys.api.EventBus;
import at.ac.tuwien.infosys.api.domain.Neighbour;
import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.PolicyConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorConfig;
import at.ac.tuwien.infosys.api.domain.SensorData;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.Policy;
import at.ac.tuwien.infosys.internal.edge.ClassifierEdge;

public class KnowledgeBaseService implements KnowledgeBase, Subscriber<ReportEvent> {

	private UndirectedGraph<SensorState, ClassifierEdge> graph;
	private List<SensorData> snapshot;
	private PanoptesConfiguration panoptesConfig;
	private SensorConfig config;

	private List<ReportEvent> reports;
	private EventBus eventBus;

	private Map<Class<? extends Policy>, PolicyConfiguration> policies;

	public KnowledgeBaseService() {
		super();
		policies = new HashMap<Class<? extends Policy>, PolicyConfiguration>();
		graph = new SimpleGraph<SensorState, ClassifierEdge>(ClassifierEdge.class);
		panoptesConfig = new PanoptesConfiguration();
		reset();
	}

	private void reset() {
		reports = new ArrayList<ReportEvent>();
		panoptesConfig.reset();
		config = null;
	}

	@Override
	public void reset(boolean full) {
		reset();
		if (full) {
			panoptesConfig.setCurrent(0);
			graph = new SimpleGraph<SensorState, ClassifierEdge>(ClassifierEdge.class);
		} else {
			for (SensorState state : this.loadCurrentState()) {
				state.reset(true);
			}
		}
	}

	public void init() {
		eventBus.subscribe(ReportEvent.class, this);
	}

	public void destroy() {
		eventBus.unsubscribe(ReportEvent.class, this);
	}

	@Override
	public void connect(SensorState data1, SensorState data2, Classifier classifier1, Classifier classifier2) {
		synchronized (this) {
			graph.addVertex(data1);
			graph.addVertex(data2);
			ClassifierEdge edge = new ClassifierEdge(classifier1, classifier2);
			graph.addEdge(data1, data2, edge);
		}
	}

	@Override
	public SensorState loadSensorState(String sensor) {
		for (SensorState state : graph.vertexSet()) {
			if (state.getSensor().equals(sensor)) {
				return state;
			}
		}
		return null;
	}

	@Override
	public void persistSnapshot(List<SensorData> sensorData) {
		this.snapshot = sensorData;
	}

	@Override
	public List<SensorData> loadSnapshot() {
		if (snapshot == null)
			return new ArrayList<SensorData>();
		return snapshot;
	}

	@Override
	public void persistSensorState(SensorState state) {
		graph.addVertex(state);
	}

	@Override
	public List<SensorState> loadCurrentState() {
		return new ArrayList<SensorState>(graph.vertexSet());
	}

	@Override
	public List<Neighbour> loadNeighbours(SensorState state) {
		List<Neighbour> neighbours = new ArrayList<Neighbour>();
		int classIdx = 0;
		for (ClassifierEdge edge : graph.edgesOf(state)) {
			SensorState tail = null;
			Classifier classifier = null;
			if (graph.getEdgeTarget(edge).equals(state)) {
				tail = graph.getEdgeSource(edge);
				classifier = edge.getClassifier2();
				classIdx = 0;
			} else {
				tail = graph.getEdgeTarget(edge);
				classifier = edge.getClassifier1();
				classIdx = 1;
			}
			Neighbour neighbour = new Neighbour();
			neighbour.setClassIdx(classIdx);
			neighbour.setClassifier(classifier);
			neighbour.setState(tail);
			neighbours.add(neighbour);

		}
		return neighbours;
	}

	@Override
	public void persistSensorConfig(SensorConfig sensorConfig) {
		this.config = sensorConfig;
	}

	@Override
	public SensorConfig loadSensorConfig() {
		return this.config;
	}

	@Override
	public void persistPanoptesConfig(PanoptesConfiguration panoptesConfig) {
		this.panoptesConfig = panoptesConfig;
	}

	@Override
	public PanoptesConfiguration loadPanoptesConfig() {
		return panoptesConfig;
	}

	@Override
	public PolicyConfiguration loadPolicies(Class<? extends Policy> policyClass) {
		return this.policies.get(policyClass);
	}

	@Override
	public void persistPolicies(Class<? extends Policy> policyClass, List<Class<?>> policies) {
		PolicyConfiguration config = new PolicyConfiguration();
		config.setPolicies(policies);
		this.policies.put(policyClass, config);
	}

	@Override
	public String loadSelectedPolicy(Class<? extends Policy> policyClass) {
		return this.loadPolicies(policyClass).getSelectedPolicy();
	}

	@Override
	public void notify(ReportEvent event) {
		reports.add(event);
	}

	@Override
	public List<ReportEvent> loadReports() {
		return this.reports;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}
}
