package at.ac.tuwien.infosys.internal.edge;

import org.jgrapht.graph.DefaultEdge;

import weka.classifiers.Classifier;

public class ClassifierEdge extends DefaultEdge {

	private static final long serialVersionUID = 1L;
	
	private Classifier classifier1;
	private Classifier classifier2;

	public ClassifierEdge(Classifier classifier1, Classifier classifier2) {
		this.classifier1 = classifier1;
		this.classifier2 = classifier2;
	}
	
	public Classifier getClassifier2() {
		return classifier2;
	}

	public void setClassifier2(Classifier classifier2) {
		this.classifier2 = classifier2;
	}

	public Classifier getClassifier1() {
		return classifier1;
	}

	public void setClassifier1(Classifier classifier1) {
		this.classifier1 = classifier1;
	}
}