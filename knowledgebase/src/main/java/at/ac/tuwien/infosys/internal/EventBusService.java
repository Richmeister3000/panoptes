package at.ac.tuwien.infosys.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.tuwien.infosys.api.EventBus;
import at.ac.tuwien.infosys.api.events.Event;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;

public class EventBusService implements EventBus {

	private Map<Class<?>, List<Subscriber>> subscriberMap;

	public EventBusService() {
		subscriberMap = new HashMap<Class<?>, List<Subscriber>>();
	}

	@Override
	public void unsubscribe(Class<? extends Event> clazz, Subscriber<?> subscriber) {
		subscriberMap.get(clazz).remove(subscriber);
	}

	@Override
	public void subscribe(Class<? extends Event> clazz, Subscriber<?> subscriber) {
		List<Subscriber> subscriberList = null;
		if (subscriberMap.containsKey(clazz)) {
			subscriberList = subscriberMap.get(clazz);
		} else {
			subscriberList = new ArrayList<Subscriber>();
		}
		subscriberList.add(subscriber);
		subscriberMap.put(clazz, subscriberList);
	}

	@Override
	public void publish(final Event event) {
		if (subscriberMap.containsKey(event.getClass())) {
			for (final Subscriber sub : subscriberMap.get(event.getClass())) {
				new Thread(new Runnable() {
					public void run() {
						sub.notify(event);
					}
				}).start();
			}
		}
		if (subscriberMap.containsKey(Event.class)) {
			for (final Subscriber sub : subscriberMap.get(Event.class)) {
				new Thread(new Runnable() {
					public void run() {
						sub.notify(event);
					}
				}).start();
			}
		}
		if (!event.getClass().equals(PlainEvent.class)) {
			this.publish(new PlainEvent(event.getClass().getSimpleName()));
		}
	}
}
