package at.ac.tuwien.infosys.internal;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class SensorDataProvider implements IDataProvider<String> {

	private static final long serialVersionUID = 1L;

	IModel<String> model;

	private List<String> sensors;

	public SensorDataProvider(List<String> sensors) {
		this.sensors = sensors;
	}
	
	@Override
	public void detach() {

	}

	@Override
	public int size() {
		return sensors.size();
	}

	@Override
	public Iterator<? extends String> iterator(int first, int count) {
		return sensors.subList(first, first + count).iterator();
	}

	@Override
	public IModel<String> model(String object) {
		int idx = sensors.indexOf(object);
		if (idx >= 0)
			return new Model<String>(sensors.get(idx));
		return null;
	}
}
