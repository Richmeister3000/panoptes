package at.ac.tuwien.infosys.internal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.time.Duration;
import org.ops4j.pax.wicket.api.PaxWicketBean;

import at.ac.tuwien.infosys.api.domain.PolicyConfiguration;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.TrainEvent;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;
import at.ac.tuwien.infosys.api.policy.AdaptationPolicy;
import at.ac.tuwien.infosys.api.policy.FrequencyPolicy;
import at.ac.tuwien.infosys.api.policy.SampleSizePolicy;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	@PaxWicketBean(name = "knowledgeBaseService")
	private transient KnowledgeBase knowledgeBaseService;

	public HomePage(final PageParameters parameters) {

		List<ITab> tabs = new ArrayList<ITab>();
		tabs.add(new AbstractTab(new Model<String>("Monitor")) {

			private static final long serialVersionUID = 1L;

			public Panel getPanel(String panelId) {
				return new MonitorPanel(panelId);
			};
		});
		tabs.add(new AbstractTab(new Model<String>("Chart")) {

			private static final long serialVersionUID = 1L;

			public Panel getPanel(String panelId) {
				return new ChartPanel(panelId);
			};
		});
		tabs.add(new AbstractTab(new Model<String>("Report")) {
			
			private static final long serialVersionUID = 1L;

			public Panel getPanel(String panelId) {
				return new ReportPanel(panelId);
			}
		});

		add(new AjaxTabbedPanel("tabs", tabs));
		
		WebMarkupContainer container = new WebMarkupContainer("config");
		container.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(10)));
		container.setOutputMarkupId(true);
		container.setOutputMarkupPlaceholderTag(true);

		PanoptesConfigurationModel config = new PanoptesConfigurationModel();

		Label deviation = new Label("deviation", new PropertyModel<String>(config, "deviation"));
		Label training = new Label("training", new PropertyModel<String>(config, "training"));
		Label sampleSize = new Label("sampleSize", new PropertyModel<String>(config, "sampleSize"));
		Label frequency = new Label("frequency", new PropertyModel<String>(config, "frequency"));
		Label measurements = new Label("measurements", new PropertyModel<String>(config, "measurements"));

		container.add(sampleSize);
		container.add(training);
		container.add(deviation);
		container.add(frequency);
		container.add(measurements);
		add(container);

		AjaxButton trainBtn = new AjaxButton("train") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				TrainEvent event = new TrainEvent();
				event.setArffStream(this.getClass().getResourceAsStream("/ARFF/Training2504Dev100.arff"));
//				event.setArffStream(this.getClass().getResourceAsStream("/ARFF/Training2504Dev20.arff"));
//				event.setArffStream(this.getClass().getResourceAsStream("/ARFF/SmallTraining.arff"));
				knowledgeBaseService.getEventBus().publish(event);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
			}
		};
		AjaxButton startBtn = new AjaxButton("start") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				knowledgeBaseService.getEventBus().publish(new PlainEvent("StartEvent"));
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
			}
		};
		AjaxButton stopBtn = new AjaxButton("stop") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				knowledgeBaseService.getEventBus().publish(new PlainEvent("StopEvent"));
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
			}
		};
		AjaxButton resetBtn = new AjaxButton("reset") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				knowledgeBaseService.reset(false);
				knowledgeBaseService.getEventBus().publish(new PlainEvent("StopEvent"));
				knowledgeBaseService.getEventBus().publish(new PlainEvent("ResetEvent"));
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
			}
		};
		
		Form<?> actionForm = new Form<Void>("actionForm");
		
		actionForm.add(trainBtn);
		actionForm.add(startBtn);
		actionForm.add(stopBtn);
		actionForm.add(resetBtn);
		add(actionForm);

		AjaxButton submitBtn = new AjaxButton("submit") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				knowledgeBaseService.getEventBus().publish(new PlainEvent("PolicyEvent"));
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
			}
		};

		PolicyConfiguration freqConfig = knowledgeBaseService.loadPolicies(FrequencyPolicy.class);
		DropDownChoice<String> frequencyPolicy = new DropDownChoice<String>("frequency", new PropertyModel<String>(
				freqConfig, "selectedPolicy"), freqConfig.getPoliciesAsString());

		PolicyConfiguration sampleConfig = knowledgeBaseService.loadPolicies(SampleSizePolicy.class);
		DropDownChoice<String> sampleSizePolicy = new DropDownChoice<String>("sampleSize", new PropertyModel<String>(
				sampleConfig, "selectedPolicy"), sampleConfig.getPoliciesAsString());

		PolicyConfiguration adaptConfig = knowledgeBaseService.loadPolicies(AdaptationPolicy.class);
		DropDownChoice<String> adaptationPolicy = new DropDownChoice<String>("adaptation", new PropertyModel<String>(
				adaptConfig, "selectedPolicy"), adaptConfig.getPoliciesAsString());


		Form<?> form = new Form<Void>("form");

		TextField<String> maxError = new TextField<String>("maxError", new PropertyModel<String>(config, "maxError"));
		form.add(maxError);
		
		form.add(frequencyPolicy);
		form.add(sampleSizePolicy);
		form.add(adaptationPolicy);
		form.add(submitBtn);

		add(form);
	}

	public class PanoptesConfigurationModel implements Serializable {

		private static final long serialVersionUID = 1L;

		public String getDeviation() {
			return String.valueOf(Math.round(knowledgeBaseService.loadPanoptesConfig().getDeviation() * 100) / 100D);
		}

		public String getFrequency() {
			return String.valueOf(knowledgeBaseService.loadPanoptesConfig().getFrequency());
		}

		public String getSampleSize() {
			return String.valueOf(knowledgeBaseService.loadPanoptesConfig().getSampleSize());
		}

		public String getTraining() {
			return String
					.valueOf(Math.round((Integer.valueOf(knowledgeBaseService.loadPanoptesConfig().getCurrent()).doubleValue() / knowledgeBaseService.loadPanoptesConfig().getMax()) * 100D));
		}

		public String getMeasurements() {
			return String.valueOf(knowledgeBaseService.loadPanoptesConfig().getMeasurements());
		}

		public String getMaxError() {
			return String.valueOf(knowledgeBaseService.loadPanoptesConfig().getMaxError());
		}

		public void setMaxError(String maxError) {
			knowledgeBaseService.loadPanoptesConfig().setMaxError(Double.valueOf(maxError));
		}
	}
}
