package at.ac.tuwien.infosys.internal;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Chart extends NonCachingImage  {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Chart.class);
	private int width;
	private int height;
	private ChartRenderingInfo chartRenderingInfo = new ChartRenderingInfo(new StandardEntityCollection());

	public Chart(String id, IModel<JFreeChart> model, int width, int height) {
		super(id, model);
		this.width = width;
		this.height = height;
	}

	@Override
	protected IResource getImageResource() {
		IResource imageResource = null;
		final JFreeChart chart = (JFreeChart) getDefaultModelObject();
		imageResource = new DynamicImageResource() {
			private static final long serialVersionUID = 1L;

			@Override
			protected byte[] getImageData(Attributes attributes) {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				try {
					if (chart != null) {
						chartRenderingInfo.clear();
						ChartUtilities.writeChartAsPNG(stream, chart, width, height, chartRenderingInfo);
					}
				} catch (IOException ex) {
					LOGGER.error("Error occured while creating chart", ex);
				}
				return stream.toByteArray();
			}
		};
		return imageResource;
	}
}
