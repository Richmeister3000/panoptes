package at.ac.tuwien.infosys.internal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.util.time.Duration;
import org.ops4j.pax.wicket.api.PaxWicketBean;

import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class MonitorPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@PaxWicketBean(name = "knowledgeBaseService")
	private transient KnowledgeBase knowledgeBaseService;

	public MonitorPanel(String id) {
		super(id);

		IModel<List<SensorState>> list = new LoadableDetachableModel<List<SensorState>>() {
			private static final long serialVersionUID = 1L;

			@Override
			protected List<SensorState> load() {
				return knowledgeBaseService.loadCurrentState();
			}
		};

		WebMarkupContainer listContainer = new WebMarkupContainer("container");
		listContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)));
		listContainer.add(new ListView<SensorState>("list", list) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SensorState> item) {
				SensorState state = item.getModelObject();

				item.add(new Label("sensor", state.getSensor()));
				item.add(new Label("measurement", String.valueOf(state.getLastMeasurement())));

				Double calc = state.calculationMean();
				Double error = state.getError();

				item.add(new Label("calculation", calc != null ? String.valueOf(Math.round(calc * 100) / 100D) : "N/A"));
				item.add(new Label("error", String.valueOf(Math.round(error * 100) / 100D)));

				SimpleDateFormat sf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
				item.add(new Label("lastMeasurement", sf.format(new Date(state.getLastMeasured()))));
				item.add(new Label("lastCalculation", sf.format(new Date(state.getLastCalculated()))));
				item.add(new Label("measured", String.valueOf(state.getMeasured())));
			}
		});

		listContainer.setOutputMarkupId(true);
		listContainer.setOutputMarkupPlaceholderTag(true);

		add(listContainer);
	}
}