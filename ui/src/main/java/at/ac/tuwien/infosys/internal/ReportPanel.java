package at.ac.tuwien.infosys.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.util.time.Duration;
import org.ops4j.pax.wicket.api.PaxWicketBean;

import at.ac.tuwien.infosys.api.events.ReportEvent;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class ReportPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@PaxWicketBean(name = "knowledgeBaseService")
	private transient KnowledgeBase knowledgeBaseService;

	public ReportPanel(String id) {
		super(id);
		
		IModel<List<ReportEvent>> list = new LoadableDetachableModel<List<ReportEvent>>() {
			
			private static final long serialVersionUID = 1L;

			@Override
			protected List<ReportEvent> load() {
				List<ReportEvent> list = new ArrayList<ReportEvent>(knowledgeBaseService.loadReports());
				Collections.reverse(list);
				return list;
			}
		};
		
		WebMarkupContainer listContainer = new WebMarkupContainer("container");
		listContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(10)));
		listContainer.add(new ListView<ReportEvent>("list", list) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<ReportEvent> item) {
				ReportEvent report = item.getModelObject();
				item.add(new Label("report", report.toString()));
			}
		});

		listContainer.setOutputMarkupId(true);
		listContainer.setOutputMarkupPlaceholderTag(true);

		add(listContainer);
	}
}
