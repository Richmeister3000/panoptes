package at.ac.tuwien.infosys.internal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import weka.classifiers.Classifier;
import weka.classifiers.UpdateableClassifier;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.SimpleLinearRegression;
import weka.classifiers.lazy.LWL;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

public class RegressionTest {
	
	@Test
	public void regressiontest() throws Exception {
		ConnectorTask connectorTask = new ConnectorTask(null, null, null, null);

		InputStream arffStream = this.getClass().getResourceAsStream("/ARFF/Correlation.arff");
		BufferedReader reader = new BufferedReader(new InputStreamReader(arffStream));
		ArffReader arff = new ArffReader(reader);
		Instances trainingSet = arff.getData();

		FastVector attributes = new FastVector();
		Attribute attr1 = new Attribute("a");
		attributes.addElement(attr1);

		List<Instances> instList = new ArrayList<Instances>();
		for (int i = 0; i < trainingSet.numAttributes(); i++) {
			instList.add(new Instances("rel", attributes, trainingSet.numInstances()));
		}

		for (int i = 0; i < trainingSet.numInstances(); i++) {
			for (int x = 0; x < trainingSet.numAttributes(); x++) {
				Instance instance = new Instance(1);
				instance.setValue(0, trainingSet.instance(i).value(x));
				instList.get(x).add(instance);
			}
		}

		// build correlation function from a to e
		Classifier classifier = connectorTask.buildClassifier(instList.get(0), instList.get(2));
		System.out.println(classifier);

	}

	@Test
	public void incrementaltest() throws Exception {
		InputStream arffStream = this.getClass().getResourceAsStream("/ARFF/AdditiveCorrelation.arff");
		BufferedReader reader = new BufferedReader(new InputStreamReader(arffStream));
		ArffReader arff = new ArffReader(reader);
		Instances trainingSet = arff.getData();
		trainingSet.setClassIndex(1);

		LWL lwl = new LWL();
		lwl.setClassifier(new LinearRegression());
		lwl.setWeightingKernel(3); // inverse euclidean distance
		lwl.buildClassifier(trainingSet);

		System.out.println(lwl.toString());

		Instance instance = new Instance(2);
		instance.setValue(0, 20);
		instance.setDataset(trainingSet);

		double b = Math.ceil(lwl.classifyInstance(instance));
		Assert.assertEquals(0.0, b);

		Instance instance1 = new Instance(2);
		instance1.setValue(0, 1);
		instance1.setValue(1, 2);
		instance1.setDataset(trainingSet);
		Instance instance2 = new Instance(2);
		instance2.setValue(0, 2);
		instance2.setValue(1, 4);
		instance2.setDataset(trainingSet);
		Instance instance3 = new Instance(2);
		instance3.setValue(0, 3);
		instance3.setValue(1, 6);
		instance3.setDataset(trainingSet);
		Instance instance4 = new Instance(2);
		instance4.setValue(0, 4);
		instance4.setValue(1, 8);
		instance4.setDataset(trainingSet);
		Instance instance5 = new Instance(2);
		instance5.setValue(0, 5);
		instance5.setValue(1, 10);
		instance5.setDataset(trainingSet);
		Instance instance6 = new Instance(2);
		instance6.setValue(0, 6);
		instance6.setValue(1, 12);
		instance6.setDataset(trainingSet);
		Instance instance7 = new Instance(2);
		instance7.setValue(0, 7);
		instance7.setValue(1, 14);
		instance7.setDataset(trainingSet);

		((UpdateableClassifier) lwl).updateClassifier(instance1);
		((UpdateableClassifier) lwl).updateClassifier(instance2);
		((UpdateableClassifier) lwl).updateClassifier(instance3);
		((UpdateableClassifier) lwl).updateClassifier(instance4);
		((UpdateableClassifier) lwl).updateClassifier(instance5);
		((UpdateableClassifier) lwl).updateClassifier(instance6);
		((UpdateableClassifier) lwl).updateClassifier(instance7);

		b = lwl.classifyInstance(instance);
		Assert.assertEquals(40, Math.round(b));
	}

	@Test
	public void simpleLinearRegressiontest() throws Exception {
		InputStream arffStream = this.getClass().getResourceAsStream("/ARFF/SimpleCorrelation.arff");
		BufferedReader reader = new BufferedReader(new InputStreamReader(arffStream));
		ArffReader arff = new ArffReader(reader);
		Instances trainingSet = arff.getData();
		trainingSet.setClassIndex(0);

		SimpleLinearRegression classifier = new SimpleLinearRegression();
		classifier.buildClassifier(trainingSet);

		System.out.println(classifier.toString());

		Instance instance = new Instance(2);
		instance.setValue(1, 20);
		instance.setDataset(trainingSet);

		double b = Math.ceil(classifier.classifyInstance(instance));
		Assert.assertEquals(10.0, b);
		System.out.println(classifier.getIntercept());
	}

	@Test
	public void hardLinearRegressiontest() throws Exception {
		InputStream arffStreamA = this.getClass().getResourceAsStream("/ARFF/A.arff");
		BufferedReader readerA = new BufferedReader(new InputStreamReader(arffStreamA));
		ArffReader arffA = new ArffReader(readerA);
		Instances trainingSetA = arffA.getData();

		InputStream arffStreamB = this.getClass().getResourceAsStream("/ARFF/B.arff");
		BufferedReader readerB = new BufferedReader(new InputStreamReader(arffStreamB));
		ArffReader arffB = new ArffReader(readerB);
		Instances trainingSetB = arffB.getData();

		FastVector attributes = new FastVector();
		Attribute attr1 = new Attribute("a");
		Attribute attr2 = new Attribute("b");
		attributes.addElement(attr1);
		attributes.addElement(attr2);

		int min = Math.min(trainingSetA.numInstances(), trainingSetB.numInstances());
		Instances instances = new Instances("rel", attributes, min * 2);
		instances.setClassIndex(1);

		for (int i = 0; i < min; i++) {
			for (int j = 0; j < min; j++) {
				Instance instance = new Instance(2);
				instance.setValue(0, trainingSetA.instance(i).value(0));
				instance.setValue(1, trainingSetB.instance(i).value(0));
				instances.add(instance);
			}
		}

		Classifier classifier = new LinearRegression();
		classifier.buildClassifier(instances);

		System.out.println(classifier.toString());

		Instance instance = new Instance(2);
		instance.setValue(0, 7);
		instance.setDataset(instances);

		double b = Math.ceil(classifier.classifyInstance(instance));
		Assert.assertEquals(10.0, b);
	}
}
