package at.ac.tuwien.infosys.internal;

import junit.framework.Assert;

import org.junit.Test;

public class AnalyzeServiceTest {

	@Test
	public void graphsizetest() throws Exception {
		AnalyzeService service = new AnalyzeService();
		
		int graphsize = service.graphsize(4);
		Assert.assertEquals(6, graphsize);
		
		graphsize = service.graphsize(5);
		Assert.assertEquals(10, graphsize);
		
		graphsize = service.graphsize(6);
		Assert.assertEquals(15, graphsize);
		
		graphsize = service.graphsize(24);
		Assert.assertEquals(276, graphsize);
	}
}
