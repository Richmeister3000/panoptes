package at.ac.tuwien.infosys.internal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import at.ac.tuwien.infosys.api.domain.Neighbour;
import at.ac.tuwien.infosys.api.domain.PanoptesConfiguration;
import at.ac.tuwien.infosys.api.domain.SensorData;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.events.PlainEvent;
import at.ac.tuwien.infosys.api.events.Subscriber;
import at.ac.tuwien.infosys.api.events.TrainEvent;
import at.ac.tuwien.infosys.api.mapek.Analyze;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class AnalyzeService implements Analyze, Subscriber<TrainEvent> {

	private KnowledgeBase knowledgeBase;

	private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzeService.class);
	private static final int MAX_THREAD_SIZE = 5;

	private List<Thread> threadpool;

	public AnalyzeService() {
		threadpool = new ArrayList<Thread>();
	}

	public void init() {
		// subscription for events
		knowledgeBase.getEventBus().subscribe(TrainEvent.class, this);
	}

	@Override
	public void notify(TrainEvent event) {
		InputStream arffStream = event.getArffStream();
		if (arffStream != null) {
			knowledgeBase.reset(true);
			this.train(arffStream);
		} else {
			LOGGER.info("Empty Stream!");
		}
	}

	public void train(InputStream arffStream) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(arffStream));
			ArffReader arff = new ArffReader(reader);
			Instances trainingSet = arff.getData();

			FastVector attributes = new FastVector();
			Attribute attr1 = new Attribute("a");
			attributes.addElement(attr1);

			Map<SensorState, Instances> instances = new Hashtable<SensorState, Instances>();

			for (int i = 0; i < trainingSet.numAttributes(); i++) {
				SensorState state = new SensorState();
				state.setSensor(trainingSet.attribute(i).name());
				Instances insts = new Instances("rel", attributes, trainingSet.numInstances());
				for (int x = 0; x < trainingSet.numInstances(); x++) {
					Instance instance = new Instance(1);
					instance.setValue(0, trainingSet.instance(x).value(i));
					insts.add(instance);
				}
				instances.put(state, insts);
			}

			// build graph
			List<SensorState> sensorStates = new ArrayList<SensorState>(instances.keySet());
			int graphsize = graphsize(sensorStates.size());
			PanoptesConfiguration panoptesConfig = knowledgeBase.loadPanoptesConfig();
			panoptesConfig.setMax(graphsize);

			for (int i = 0; i < sensorStates.size(); i++) {
				for (int j = i + 1; j < sensorStates.size(); j++) {
					Thread t = new Thread(new ConnectorTask(knowledgeBase, instances, sensorStates.get(i),
							sensorStates.get(j)));
					t.start();

					threadpool.add(t);
					if ((threadpool.size() % MAX_THREAD_SIZE) == 0) {
						waitForThreads();
					}
				}
			}
			waitForThreads();

			knowledgeBase.getEventBus().publish(new PlainEvent("FinishedTrainEvent"));

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
			return;
		}
	}

	private void waitForThreads() throws Exception {
		for (Thread thread : threadpool) {
			thread.join();
		}
		threadpool.clear();
	}

	public int graphsize(int n) {
		int sum = 0;
		for (int i = n - 1; i > 0; i--) {
			sum += i;
		}
		return sum;
	}

	public void analyze() {
		try {
			List<SensorState> measuredVertices = new ArrayList<SensorState>();
			for (SensorData data : this.knowledgeBase.loadSnapshot()) {
				measuredVertices.add(this.knowledgeBase.loadSensorState(data.getName()));
			}

			long time = new Date().getTime();
			for (SensorState source : measuredVertices) {
				List<Neighbour> neighbours = this.knowledgeBase.loadNeighbours(source);
				for (Neighbour neighbour : neighbours) {
					Instance instance = new Instance(2);
					instance.setValue(0, source.getLastMeasurement());
					
					double calculatedValue = neighbour.getClassifier().classifyInstance(instance);
					
					SensorState target = neighbour.getState();
					target.addCalcMeasurment(calculatedValue);
					target.setLastCalculated(time);
					this.knowledgeBase.persistSensorState(target);
				}
			}

			for (SensorState source : measuredVertices) {
				SensorState state = this.knowledgeBase.loadSensorState(source.getSensor());
				double value = 0;
				for (double calc : state.getCalculation()) {
					value += state.getLastMeasurement() - calc;
				}
				if (state.getCalculation().size() > 0) {
					state.addFailure(Math.abs(value / state.getCalculation().size()));
				}
			}

			double failure = 0;
			List<SensorState> vertices = this.knowledgeBase.loadCurrentState();
			for (SensorState state : vertices) {
				failure += state.getError();
			}

			PanoptesConfiguration panoptesConfig = this.knowledgeBase.loadPanoptesConfig();
			panoptesConfig.setDeviation(failure / vertices.size());
			this.knowledgeBase.persistPanoptesConfig(panoptesConfig);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public KnowledgeBase getKnowledgeBase() {
		return knowledgeBase;
	}

	public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}
}
