package at.ac.tuwien.infosys.internal;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import at.ac.tuwien.infosys.api.domain.SensorState;
import at.ac.tuwien.infosys.api.mapek.KnowledgeBase;

public class ConnectorTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectorTask.class);

    private KnowledgeBase knowledgeBase;
    private Map<SensorState, Instances> instances;
    private SensorState source;
    private SensorState target;

    public ConnectorTask(KnowledgeBase knowledgeBase, Map<SensorState, Instances> instances, SensorState source,
                    SensorState target) {
            this.knowledgeBase = knowledgeBase;
            this.instances = instances;
            this.source = source;
            this.target = target;
    }

    public void run() {
            try {
                    Classifier classifier1 = this.buildClassifier(instances.get(source), instances.get(target));
                    Classifier classifier2 = this.buildClassifier(instances.get(target), instances.get(source));
                    knowledgeBase.connect(source, target, classifier1, classifier2);
                    knowledgeBase.loadPanoptesConfig().connected();

            } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                    e.printStackTrace();
            }
    }

    public Classifier buildClassifier(Instances a, Instances b) throws Exception {
            FastVector attributes = new FastVector();
            Attribute attr1 = new Attribute("a");
            Attribute attr2 = new Attribute("b");
            attributes.addElement(attr1);
            attributes.addElement(attr2);

            int min = Math.min(a.numInstances(), b.numInstances());
            Instances instances = new Instances("rel", attributes, min * min);
            instances.setClassIndex(1);

            for (int i = 0; i < min; i++) {
                            Instance instance = new Instance(2);
                            instance.setValue(0, a.instance(i).value(0));
                            instance.setValue(1, b.instance(i).value(0));
                            instances.add(instance);
            }

            Classifier classifier = new LinearRegression();
            classifier.buildClassifier(instances);
            return classifier;
    }
}